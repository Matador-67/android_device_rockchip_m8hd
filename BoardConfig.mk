#
# Copyright (C) 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
DEVICE_FOLDER := device/rockchip/u8
-include device/rockchip/pipo-common/BoardConfigCommon.mk

TARGET_BOOTLOADER_BOARD_NAME := pipo_m8hd
TARGET_MANUFACTOR := pipo

TARGET_OTA_ASSERT_DEVICE := m8hd,pipo_m8hd

BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := $(DEVICE_FOLDER)/bluetooth

#TARGET_USES_AKMSENSOR := true

# Build
TARGET_SYSTEMIMAGE_USE_SQUISHER := true

# Hardware tunables framework
#BOARD_HARDWARE_CLASS := device/rockchip/m8hd/cmhw/

#TARGET_KERNEL_CONFIG := cyanogenmod_m8hd_defconfig
TARGET_PREBUILT_RECOVERY_KERNEL := $(DEVICE_FOLDER)/kernel
TARGET_PREBUILT_KERNEL := $(DEVICE_FOLDER)/kernel
ifeq ($(TARGET_PREBUILT_KERNEL),)
	LOCAL_KERNEL := $(DEVICE_FOLDER)/kernel
else
	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif

PRODUCT_COPY_FILES += \
    $(LOCAL_KERNEL):kernel
